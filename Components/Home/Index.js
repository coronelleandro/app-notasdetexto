import { getActionFromState,useFocusEffect } from '@react-navigation/native';
import React,{useState, useEffect,useCallback,useRef} from 'react';
import { View, Text, TouchableOpacity ,StyleSheet,FlatList,Modal,TextInput,Animated,Image} from 'react-native';
import { openDatabase } from 'react-native-sqlite-storage';
import { Icon } from 'react-native-elements';
import  RenderNote  from './RenderNote'
var db = openDatabase({ name: 'UserDatabase.db' });

function HomeScreen({navigation}) {
    const [listNotes,setListNotes] = useState(null)
    const [searchValue,setSearchValue] = useState('')
    const [visibleSearch,setVisibleSearch] = useState(true)
    const [value] = useState(new Animated.Value(0))

    const selectNote = () =>{
      db.transaction(tx => {
        tx.executeSql(
          'SELECT * FROM Note ;',[],
          (tx,results) => {
            let notes = []
            for(let i=0;i<results.rows.length;i++){
              notes.push(results.rows.item(i))
            }
            setListNotes(notes)
          }
        );
      });
    };
  
    useFocusEffect (
      useCallback (() => {  
        setSearchValue('');
        db.transaction(tx => {
          tx.executeSql(
            'CREATE TABLE IF NOT EXISTS Note ( id INTEGER NOT NULL, title TEXT NOT NULL, noteText TEXT NOT NULL, dateNote TEXT NOT NULL, primary key(id));',[],
            () => console.log('crear Note'),
            (_, error) => console.log('error Note')
          );
        });
        
        /*db.transaction(tx => {
          tx.executeSql(
            'DROP TABLE IF EXISTS Note;',[],
            () => console.log('drop Note'),
            (_, error) => console.log('error Note')
          );
        });*/
        selectNote()
        setVisibleSearch(true)
      },[] ) 
    ) ;

    const searchNote = (res) =>{
      setSearchValue(res)
      
      db.transaction(tx => {
        tx.executeSql(
          'SELECT * FROM Note WHERE title LIKE ?;',[`%${res}%`],
          (tx,results) => {
            let notes = []
            for(let i=0;i<results.rows.length;i++){
              notes.push(results.rows.item(i))
            }
            setListNotes(notes)
          }
        );
      });
    }

    const note = () =>{
      navigation.navigate('Note',{
        notes : listNotes
      })
    }

    const onBlur = () => {
      
    }

  
    return (
      <>
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor:'#A200FF'}}>
        <View style={{flex:(visibleSearch ? 0.6 : 0),width:'100%',}}>
          <Animated.View style={{display:(visibleSearch ? 'flex' : 'none'),width:'100%',flex:1,justifyContent:'center',alignItems:'center',position:'relative'}}>
            <Image source={require('../../Images/logo1.png')}
						style={{position:'absolute',  width: 100 , resizeMode: 'contain' ,height:100,top:'25%'}}/>
            
          </Animated.View>
          {(!visibleSearch ?
            <View style={styles.search}>
              <TouchableOpacity style={styles.buttonBackSearch} onPress={()=>setVisibleSearch(true)}>
                <Icon name='angle-left' type='font-awesome' color='#000000' size={40}/>
              </TouchableOpacity>
              
              <TextInput placeholder='Buscar' onBlur={()=>onBlur()} onChangeText={(res)=>searchNote(res)} style={styles.inputSearch} value={searchValue}>
              </TextInput>
              
              <View style={styles.buttonSearch} >
                <Icon name='search' type='font-awesome' color='#000' size={30}/>
              </View> 
            </View>
          : 
            <View style={{width:'90%',height:50,flexDirection:'row',justifyContent:'space-between',alignItems:'center',position:'absolute',bottom:20,right:'5%'}}>
              <Text style={styles.titleText}>Notas de Texto</Text>
              <TouchableOpacity style={{width:60,height:'100%',justifyContent:'center',alignItems:'flex-end'}} onPress={()=>setVisibleSearch(false)}>
                <Icon name='search' type='font-awesome' color='#ffffff' size={30}/>
              </TouchableOpacity>
            </View> 
          )}
        </View>
        <View style={{flex:1,backgroundColor:'#F9F9F9',paddingTop:30,width:'100%',justifyContent:'center',alignItems:'center',}}>
          {<FlatList
            style={styles.flatList}
            data={listNotes}
            renderItem={(note)=>(
              <RenderNote db={db} listNotes={listNotes} note={note} navigation={navigation} selectNote={selectNote}/>
            )}
            keyExtractor={(item, index) => index.toString()}
          />}
          <TouchableOpacity onPress={()=>note()} style={styles.buttonAdd}>
            <Icon name='plus' type='font-awesome' color='#ffffff' size={30}/>  
          </TouchableOpacity>
        </View>
      </View>
      </>
    );
  }

  const styles = StyleSheet.create({
    titleText:{
      fontSize:30,
      color:'#ffffff'
    },
    buttonHeader:{
      width:50,
      height:50,
      borderRadius:25,
      justifyContent:'center',  
    },
    search:{
      flexDirection: 'row',
      justifyContent: 'space-around',
      position:'relative',
      backgroundColor:'#A200FF',
      height:80,
      justifyContent:'center',
      paddingHorizontal:10,
      alignItems:'center',
    },
    buttonBackSearch:{
      width:'18%',
      height:50,
      justifyContent:'center',
      alignItems:'center',
      padding:10,
      backgroundColor:'#EEEEEE',
      borderBottomStartRadius:15,
      borderTopStartRadius:15
    },
    inputSearch:{
      width:'64%',
      backgroundColor:'rgba(0,0,0,0.07)',
      fontSize:18,
      height:50,
      backgroundColor:'#EEEEEE'
    },
    buttonSearch:{
      width:'18%',
      height:50,
      justifyContent:'center',
      alignItems:'center',
      padding:10,
      backgroundColor:'#EEEEEE',
      borderTopEndRadius:15,
      borderBottomEndRadius:15,
    }, 
    buttonAdd:{
      backgroundColor:'#A200FF',
      padding:10,
      width:68,
      height:68,
      justifyContent:'center',
      alignItems:'center',
      borderRadius:35,
      position:'absolute',
      bottom:30,
      right:'5%',
    },
    flatList:{
      width:'90%',
      flex:1,
      
    },
  })

export default HomeScreen;