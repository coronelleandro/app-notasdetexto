import { getActionFromState,useFocusEffect } from '@react-navigation/native';
import React,{useState, useEffect,useCallback,useRef} from 'react';
import { View, Text, TouchableOpacity ,StyleSheet,FlatList,Modal,TextInput,Animated,Image} from 'react-native';
import { openDatabase } from 'react-native-sqlite-storage';
import { Icon } from 'react-native-elements';

const RenderNote = (props) =>{
    const {db,listNotes,note,navigation,selectNote} = props;
    const {item} = note; 
    let {id,title,nameText,noteText,dateNote} = item;
    nameText = nameText ? nameText : title;
    const [visible,setVisible] = useState(false); 
    let cdate = new Date(dateNote)
    console.log(dateNote)
    const noteEdit = () => {
      navigation.navigate('Note',{
        notes : listNotes,
        idText:id,
        name : title,
        noteText: noteText,
      })
    }

    const cancelDelete = () => {
      setVisible(false)
    }

    const showModal = () => {
      setVisible(true)
    }

    const deleteNote = () => {
      db.transaction(tx => {
        tx.executeSql(
          'DELETE FROM Note where id=?', [id],
        );
      });
      setVisible(false)
      selectNote()
    }
   
    return (
      <>
        <TouchableOpacity onLongPress={()=>showModal()} style={styles.notes} onPress={()=>{noteEdit()}}>
          <Text style={styles.title}>{title}</Text>
          <View style={styles.dateTimeNote}>
            <Text style={styles.dateNote}>
              {(cdate.getDate()<10 ? "0"+cdate.getDate() : cdate.getDate())}/
              {(cdate.getMonth()<10 ? "0"+cdate.getMonth() : cdate.getMonth())}/
              {(cdate.getFullYear()<10 ? "0"+cdate.getFullYear() : cdate.getFullYear())}
            </Text>  
            <Text style={styles.timeNote}> 
              {(cdate.getUTCHours()<10 ? "0"+cdate.getUTCHours() : cdate.getUTCHours())}:
              {(cdate.getMinutes()<10 ? "0"+cdate.getMinutes() : cdate.getMinutes())}:
              {(cdate.getSeconds()<10 ? "0"+cdate.getSeconds() : cdate.getSeconds())}
            </Text>
          </View>
        </TouchableOpacity>

        <Modal transparent={true} visible={visible}>
          <View style={styles.modal}> 
            <View style={styles.windowModal}>
              <Text style={styles.titleModal}>¿Quieres eliminar esta nota?</Text>
              <View style={styles.buttonsModal}>
                <TouchableOpacity onPress={()=>cancelDelete()} style={styles.cancelModal}>
                  <Text style={{fontSize:25}}>cancelar</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>deleteNote()} style={styles.acceptModal}>
                  <Text style={{fontSize:25}}>aceptar</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </>
    )
  }

  const styles = StyleSheet.create({
    notes:{
      
      backgroundColor:'#ffffff',
      width:'100%',
      marginTop:18,      
      padding:20,
      borderWidth:1,
      borderRadius:15,
      borderColor:'rgba(0,0,0,0.3)'
    },
    title:{
      fontSize:20
    },
    dateTimeNote:{
      flexDirection:'row',
      marginTop:5,
    },
    dateNote:{
      fontSize:15,
      color:'#7E7E7E',
    },
    timeNote:{
      marginLeft:15,
      fontSize:15,
      color:'#7E7E7E',
    },  
    modal:{
      flex:1,
      backgroundColor:'rgba(0,0,0,0.3)',
      justifyContent:'center',
      alignItems:'center',
    },
    windowModal:{
      width:'90%',
      backgroundColor:'#ffffff',
      paddingHorizontal:20,
      paddingVertical:30,
      borderRadius:10,
    },
    titleModal:{
      fontSize:35,
      textAlign:'center',
      marginBottom:40,
    },
    buttonsModal:{
      flexDirection: 'row',
      justifyContent: 'space-around',
    },  
    cancelModal:{
      padding:10,
      borderWidth:1,
      borderRadius:15,
      borderColor:'rgba(0,0,0,0.3)'
    },
    acceptModal:{
      padding:10,
      borderWidth:1,
      borderRadius:15,
      borderColor:'rgba(0,0,0,0.3)'
    },
  })

export default RenderNote; 