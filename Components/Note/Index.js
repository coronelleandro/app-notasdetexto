import React,{useState, useEffect} from 'react';
import { StyleSheet, View, Text, TextInput, TouchableOpacity } from 'react-native';
import { initialWindowMetrics } from 'react-native-safe-area-context';
import { openDatabase } from 'react-native-sqlite-storage';
import Textarea from 'react-native-textarea';
import { Icon } from 'react-native-elements';

var db = openDatabase({ name: 'UserDatabase.db' });

function NoteScreen({navigation,route}) {
    const {notes,idText,name,noteText} = route.params;
    const [id,setId] = useState(0)
    const [title,setTitle] = useState('');
    const [text,setText] = useState('');
    const [edit,setEdit] = useState(false)
    const [init,setInit] = useState(false)
    
    useEffect(() => {
      if(name!=null || noteText!=null){
        setTitle(name)
        setText(noteText)
        setId(idText)
        setEdit(true)
      }
      else{
        setTitle('')
        setText('')
        setEdit(false)
        setId(notes.length+1)
      }
      setInit(true)
    },[])

    useEffect(() => {
    },[title,text])

    const changeTitle = (value) =>{
      setTitle(value)
    }

    const changeText = (value) =>{
      setText(value)
    }

    const saveOrUpdate = () =>{
      if(edit == true){
        updateNote()
      }
      else{
        saveNote()
      }
    }

    const backHome = () =>{
      navigation.navigate('Home')
    }

    const saveNote = () => {
      let today = new Date();
      let date = ''
      date = getDateFormat(today)
      console.log(date)          
      if(title!='' && text!=''){
        if(notes.length==0){
          db.transaction(tx => {
            tx.executeSql(
              'INSERT INTO Note (id, title , noteText, dateNote) VALUES (?,?,?,?)',[1,title,text,date],
              () => console.log('insert Note'),
              (_, error) => console.log('error Note')
            );
          });
        }
        else{
          db.transaction(tx => {
            tx.executeSql(
              'INSERT INTO Note (id, title , noteText, dateNote) VALUES (?,?,?,?)',[id,title,text,date],
              () => console.log('insert Note'),
              (_, error) => console.log('error Note')
            );
          });
        }
      }
    }

    const updateNote = () =>{
      db.transaction(tx => {
        tx.executeSql(
          'UPDATE Note SET title=?,noteText=? WHERE id=?;',[title,text,id],
          () => console.log('update Note'),
          (_, error) => console.log('error Note')
        );
      });
    }
    
    const getDateFormat = (today) => {
      let month = ''
      let day = ''
      let minutes = ''
      let hours = ''
      let seconds = ''
      let year = today.getFullYear()
      let date = ''

      month = parseInt(today.getMonth()+1)
      day = today.getDate()
      hours = today.getHours()
      minutes = today.getMinutes()
      seconds = today.getSeconds()

      if(month<10){
        month = "0"+month
      }  
      if(day<10){
        day = "0"+day
      }
  
      if(hours<10){
        hours = "0"+hours
      }
      if(minutes<10){
        minutes = "0"+minutes
      }
      if(seconds<10){
        seconds = "0"+seconds
      }

      date = ""+year+"-"+month+"-"+day+"T"+hours+":"+minutes+":"+seconds+"";
     // console.log(date)
      return date;
    } 

    return (
      <>
        <View style={{flex:1,backgroundColor:'#F9F9F9'}}>
          <View style={styles.sectionHeader}>
            <View style={styles.header}>
              <View style={styles.headerContent}>
                <TouchableOpacity style={styles.buttonBackHome} onPress={()=>backHome()}>
                  <Icon name='angle-left' type='font-awesome' color='#ffffff' size={40}/>
                </TouchableOpacity>
                <TouchableOpacity  onPress={()=>saveOrUpdate()} style={styles.submit}>
                  <Text style={styles.textButton}>GUARDAR</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View style={styles.sectionInputs}>
            <View style={styles.textInput}>
              <TextInput placeholder='Titulo' onChangeText={(res)=>changeTitle(res)} style={styles.title} value={title}>
              </TextInput>
              <TextInput 

                placeholder='Texto'
                multiline={true}
                numberOfLines={10}
                onChangeText={(res)=>changeText(res)} 
                style={styles.textArea} 
                value={text}>
              </TextInput>
            </View>
          </View>
        </View>
      </>
    );
  }

  const styles = StyleSheet.create({
    sectionHeader:{
      height:80,
      backgroundColor:'#A200FF'
    },
    header:{
      width:'100%',
      height:'100%',
      justifyContent:'center',
      backgroundColor:'#A200FF',
      paddingBottom:10,
      paddingTop:10,
    },
    headerContent:{
      width:'90%',
      flexDirection:'row',
      justifyContent:'space-between',
      marginLeft:'5%',
    },
    buttonBackHome:{
      justifyContent:'center',
      alignItems:'flex-start',
      width:60,
    },
    submit:{
      backgroundColor:'#ffffff',
      height:50,
      paddingHorizontal:20,
      justifyContent:'center',
      alignItems:'center',
      borderRadius:10,
    },
    textButton:{
      fontSize:18,
      color:'#000000',

    },
    sectionInputs:{
      flex:9,
      width:'100%',
      alignItems:'center'
    },
    textInput:{
      flex:1,
      width:'90%',
      paddingVertical:10,
    },
    title:{
      height:60,
      marginTop:10,
      width:'100%',
      borderRadius:10,
      fontSize:23,
      
    },
    textArea:{
      flex:9,
      width:'100%',
      marginTop:10,
      textAlignVertical : "top",
      borderRadius:10,
      fontSize:20,
    },
  })

export default NoteScreen;